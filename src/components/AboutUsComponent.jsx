import React from 'react'
import MyCardComponent from './MyCardComponent'

export default function AboutUsComponent() {
  return (
    <div style={{ height: '100vh'}}>
      <MyCardComponent />
    </div>
  )
}
