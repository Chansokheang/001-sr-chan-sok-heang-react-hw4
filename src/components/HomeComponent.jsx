import React from 'react'
import { Button, Card, Container } from 'react-bootstrap'

export default function HomeComponent({data}) {

    let myCard = data.map((item,param)=>(
        <Card style={{ width: '18rem' ,textAlign: 'center'}} key={param}>
            <img src={(item.thumbnail)} />
            <Card.Body>
                <Card.Title>{item.title}</Card.Title>
                <Card.Text>
                    {item.description}
                </Card.Text>
                <h1>{item.price}</h1>
                <Button variant="success">Buy the Course</Button>
            </Card.Body>
        </Card>
    ))

    return (
        <Container style={{ height: '100vh'}}>

            <h1 style={{margin: '70px 0'}}>Trending Courses</h1>
            <div className='displayCard'>
            {myCard}
            </div>
            

        </Container>
    )
}
