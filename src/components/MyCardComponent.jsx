import { Card, Container, Row } from 'react-bootstrap'
import React from 'react'

export default function MyCardComponent() {

    const txt = {
        padding: '20px 20px 0 20px'
    }
    let card = (
        <Card style={{ marginTop: '30px' }}>
            <Row>
                <div className='col-lg-6' >
                    <p className="mb-3"style={txt}>
                        Korea Software HRD Center is an academy training center for training
                        software professionals in cooperation with Korea International
                        Cooperation Agency(KOICA) and Webcash in April, 2013 in Phnom Penh,
                        Cambodia.
                    </p>
                    <p style={txt}>
                        From 2020, Korea Software HRD Center has been become
                        Global NGO with the name Foundation for Korea Software Global Aid
                        (KSGA), main sponsored by Webcash Group, to continue mission for ICT
                        Development in Cambodia and will recruit 60 t0 80 scholarship
                        students every year.
                    </p>
                </div>
                <div className='col-lg-6'>
                    <Card.Img className="border-0 rounded-0 w-100 h-100" variant="top" src="https://kshrd.com.kh/static/media/1.6169e38f.jpg" />
                </div>
            </Row>
        </Card>
    )
    let card2 = (
        <Card style={{ marginTop: '30px' }}>
            <Row>
                <div className='col-lg-6' data-aos='fade-left'>
                    <Card.Img className="border-0 rounded-0 w-100 h-100" variant="top" src="https://kshrd.com.kh/static/media/1.6169e38f.jpg" />
                </div>
                <div className='col-lg-6' data-aos='fade-right'>
                    <p className="mb-3" style={txt}>
                        Korea Software HRD Center is an academy training center for training
                        software professionals in cooperation with Korea International
                        Cooperation Agency(KOICA) and Webcash in April, 2013 in Phnom Penh,
                        Cambodia.
                    </p>
                    <p style={txt}>
                        From 2020, Korea Software HRD Center has been become
                        Global NGO with the name Foundation for Korea Software Global Aid
                        (KSGA), main sponsored by Webcash Group, to continue mission for ICT
                        Development in Cambodia and will recruit 60 t0 80 scholarship
                        students every year.
                    </p>
                </div>
            </Row>
        </Card>
    )

    return (
        <Container>
            {card2}
            {card}
            {card2}
        </Container>
    )
}

