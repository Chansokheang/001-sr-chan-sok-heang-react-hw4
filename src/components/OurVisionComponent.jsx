import React from 'react'
import { Card, Container, Row } from 'react-bootstrap'

export default function OurVisionComponent() {

  let styleCard = {
    margin: '20px',
    height: '200px',textAlign: 'center'
  }

  let card1 = (
    <Card style={styleCard}>
      <h1>Vision</h1>
      <ul>
        <li>To be the best SW Professional Traing Center in Cambodia</li>
      </ul>
  </Card>
  )
  let card2 = (
    <Card style={styleCard}>
      <h1>Mission</h1>
      <ul>
        <li>High quality training and research</li>
        <li>Develop Capacity of SW Experts to be Leaders in IT Field</li>
        <li>Developing sustainable ICT Program</li>
      </ul>
  </Card>
  )
  let card3 = (
    <Card style={styleCard}>
      <h1>Strategy</h1>
      <ul>
        <li>Best training method with up to date curriculum and enviroment</li>
        <li>Cooperation with the best IT industry to guarantee student's career and benefits</li>
        <li>Additional Soft Skill, Management. Leadership training</li>
      </ul>
  </Card>
  )
  let card4 = (
    <Card style={styleCard}>
      <h1>Slogan</h1>
      <ul>
        <li>"KSHRD", connects you to various opportunities in IT Field</li>
        <li>Raising brand awareness with contiinuous advertisement of SNS and any other media</li>
      </ul>
  </Card>
  )


  return (
    <Container style={{ height: '100vh'}}>
      <Row>
        <div className="col-lg-6">
        {card1}
        </div>
        <div className="col-lg-6">
        {card2}
        </div>
        <div className="col-lg-6">
        {card3}
        </div>
        <div className="col-lg-6">
        {card4}
        </div>
      </Row>

    </Container>
  )
}

